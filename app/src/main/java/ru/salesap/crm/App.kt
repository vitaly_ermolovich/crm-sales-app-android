package ru.salesap.crm

import android.app.Application
import android.content.Context
import androidx.multidex.MultiDex
import com.crashlytics.android.Crashlytics
import io.fabric.sdk.android.Fabric
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.singleton
import ru.salesap.crm.net.Api
import ru.salesap.crm.net.ApiRest
import ru.salesap.crm.util.SettingPrefs

class App : Application() {


    private val settingModule = Kodein.Module(name = "APP") {
        bind<Api>() with singleton { ApiRest.getApi() }
        bind<SettingPrefs>() with singleton { SettingPrefs.get(this@App) }
    }

    companion object {
        lateinit var kodein: Kodein
    }

    override fun onCreate() {
        super.onCreate()
        kodein = Kodein {
            import(settingModule)
        }
        Fabric.with(this, Crashlytics())
/*
        if (BuildConfig.DEBUG) {
            if (LeakCanary.isInAnalyzerProcess(this)) {
                return
            }
            LeakCanary.install(this)
        }
*/
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }
}
