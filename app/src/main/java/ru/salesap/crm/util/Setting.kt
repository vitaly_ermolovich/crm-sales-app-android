package ru.salesap.crm.util

import co.windly.ktxprefs.annotation.DefaultString
import co.windly.ktxprefs.annotation.Prefs

@Prefs(value = "CachePreferences")
class Setting(
    @DefaultString(value = "")
    internal val token: String,

    @DefaultString(value = "")
    internal val device: String
)