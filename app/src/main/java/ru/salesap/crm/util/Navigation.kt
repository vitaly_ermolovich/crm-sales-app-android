package ru.salesap.crm.util

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.os.IBinder
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.Fragment
import androidx.navigation.NavOptions
import androidx.navigation.fragment.findNavController
import ru.salesap.crm.R

////////////////////////////////////////////////////////////////////////////////////////////////////

fun NavOptions.Builder.setAnim(withAnim: Boolean? = true): NavOptions.Builder {
    return this.setEnterAnim(if (withAnim == true) R.anim.slide_out_right else R.anim.empty_anim)
        .setExitAnim(if (withAnim == true) R.anim.slide_in_right_slow else R.anim.empty_anim)
        .setPopEnterAnim(if (withAnim == true) R.anim.slide_out_left_fast else R.anim.empty_anim)
        .setPopExitAnim(if (withAnim == true) R.anim.slide_in_left else R.anim.empty_anim)
}

////////////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

fun Fragment.navigateSplash(
    actionId: Int, popup: Int = R.id.navigation_splash,
    bundle: Bundle? = null
) {
    this.findNavController().navigate(
        actionId, bundle,
        NavOptions.Builder()
            .setPopUpTo(
                popup,
                true
            ).build()
    )
}


/////////////////////////////////////////////////////////////////////////////////////////////////////

fun Fragment.onBack() {
    this.activity?.let { activity ->
        activity.hideKeyboard()
        if (!findNavController().navigateUp()) {
            activity.finish()
        }
    }
}

fun Fragment.hideKeyboard() {
    this.activity?.let { activity ->
        activity.currentFocus?.let { view ->
            (activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).also {
                it.hideSoftInputFromWindow(view.windowToken, 0)
            }
        }
    }
}

fun Fragment.showKeyboard(view: View) {
    this.activity?.let { activity ->
        Handler().post {
            view.requestFocus()
            (activity.getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager)
                ?.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT)
        }
    }
}

fun Fragment.hideSoftKeyboard(view: View?) {
    this.activity?.let { activity ->
        val imm = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        var token: IBinder? = view?.windowToken
        if (token == null && activity.currentFocus != null) {
            token = activity.currentFocus?.windowToken
        }
        if (token != null) {
            imm.hideSoftInputFromWindow(token, 0)
        }
    }
}

fun Activity.hideKeyboard() {
    this.let { activity ->
        activity.currentFocus?.let { view ->
            (activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).also {
                it.hideSoftInputFromWindow(view.windowToken, 0)
            }
        }
    }
}