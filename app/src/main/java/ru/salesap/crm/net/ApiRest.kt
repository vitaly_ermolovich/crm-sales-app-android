package ru.salesap.crm.net


import com.squareup.moshi.*
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import ru.salesap.crm.BuildConfig
import java.io.IOException
import java.lang.reflect.Type
import java.security.cert.X509Certificate
import java.util.*
import java.util.concurrent.TimeUnit
import javax.net.ssl.HostnameVerifier
import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManager
import javax.net.ssl.X509TrustManager

object ApiRest {

    const val TIME_OUT = 25_000L
    const val IMAGE_TIME_OUT = 45_000

    private val client =
        (if (BuildConfig.ALLOW_INVALID_CERTIFICATE) getSafeOkHttpClient() else getUnsafeOkHttpClient())
            .addInterceptor(makeLoggingInterceptor())
            .readTimeout(TIME_OUT, TimeUnit.MILLISECONDS)
            .writeTimeout(TIME_OUT, TimeUnit.MILLISECONDS)
            .connectTimeout(TIME_OUT, TimeUnit.MILLISECONDS)
            .build()

    fun getApi(): Api {

        val builder = Retrofit.Builder()
            .baseUrl(BuildConfig.BASE_URL)
            .addConverterFactory(
                MoshiConverterFactory.create(
                    Moshi.Builder()
                        .add(BaseListJsonAdapterFactory())
                        .build()
                )
            )
            .client(client)

        return builder.build().create(Api::class.java)
    }


    ////////////////////////////////////////////////////////////////////////////////

    private fun makeLoggingInterceptor(): HttpLoggingInterceptor {
        val logging = HttpLoggingInterceptor()
        logging.level =
            if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE
        return logging
    }

    ////////////////////////////////////////////////////////////////////////////////

    private fun getSafeOkHttpClient(): OkHttpClient.Builder {
        return OkHttpClient.Builder()
    }

    ////////////////////////////////////////////////////////////////////////////////

    private fun getUnsafeOkHttpClient(): OkHttpClient.Builder {
        try {
            val trustAllCerts = arrayOf<TrustManager>(object : X509TrustManager {
                override fun checkClientTrusted(
                    chain: Array<out X509Certificate>?,
                    authType: String?
                ) {
                    //
                }

                override fun checkServerTrusted(
                    chain: Array<out X509Certificate>?,
                    authType: String?
                ) {
                    //
                }

                override fun getAcceptedIssuers(): Array<X509Certificate> {
                    return arrayOf()
                }

            })
            val sslContext = SSLContext.getInstance("SSL")
            sslContext.init(null, trustAllCerts, java.security.SecureRandom())

            val sslSocketFactory = sslContext.socketFactory

            val builder = OkHttpClient.Builder()
            builder.sslSocketFactory(sslSocketFactory, trustAllCerts[0] as X509TrustManager)
            builder.hostnameVerifier(HostnameVerifier { _, _ -> true })
            return builder
        } catch (e: Exception) {
            return OkHttpClient.Builder()
        }
    }

}

class BaseListJsonAdapterFactory : JsonAdapter.Factory {
    override fun create(
        type: Type,
        annotations: Set<Annotation?>,
        moshi: Moshi
    ): JsonAdapter<*>? {
        val rawType = Types.getRawType(type)
        if (annotations.isNotEmpty()) return null
        return if (rawType == ArrayList::class.java) {
            newRealmListAdapter<Any>(type, moshi).nullSafe()
        } else null
    }

    companion object {
        private fun <T> newRealmListAdapter(type: Type, moshi: Moshi): BaseListAdapter<T> {
            val elementType = Types.collectionElementType(type, ArrayList::class.java)
            val elementAdapter: JsonAdapter<T> = moshi.adapter(elementType)
            return BaseListAdapter(elementAdapter)
        }
    }
}


class BaseListAdapter<T>(private val elementAdapter: JsonAdapter<T>) :
    JsonAdapter<ArrayList<T>?>() {

    @Throws(IOException::class)
    override fun toJson(writer: JsonWriter, value: ArrayList<T>?) {
        writer.beginArray()
        for (element in value!!) {
            elementAdapter.toJson(writer, element)
        }
        writer.endArray()
    }

    @Throws(IOException::class)
    override fun fromJson(reader: JsonReader): ArrayList<T>? {
        val result = ArrayList<T>()
        reader.beginArray()
        while (reader.hasNext()) {
            result.add(elementAdapter.fromJson(reader) ?: continue)
        }
        reader.endArray()
        return result
    }

}
