package ru.salesap.crm.net.response

open class BaseResponse<T> {
    val success: Boolean = false
    var data: T? = null
}

class ErrorResponse(val error: String?)
