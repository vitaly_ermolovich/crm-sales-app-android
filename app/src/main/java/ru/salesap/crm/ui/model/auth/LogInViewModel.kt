package ru.salesap.crm.ui.model.auth

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import ru.salesap.crm.ui.model.base.BaseViewModel
import ru.salesap.crm.ui.model.base.ViewStateStore

data class LogInViewState(
    val startAnim: Boolean = true,
    val message: String? = null
)

class LogInViewModel(private val useCase: LogInUseCase) : BaseViewModel() {

    init {
        store = ViewStateStore(LogInViewState())
        loadData()
    }

    override fun loadData() {
        getStore<ViewStateStore<LogInViewState>>().dispatchActions(useCase.startWork())
    }

    override fun getState() = getStore<ViewStateStore<LogInViewState>>().state()

}

object LogInViewModelFactory : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T =
        LogInViewModel(LogInUseCase()) as T
}
