package ru.salesap.crm.ui.fragment.base

import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_list_refresh.*
import ru.salesap.crm.R
import ru.salesap.crm.activity.base.BackResult
import ru.salesap.crm.adapter.base.BaseSupportAdapter

abstract class BaseListFragment<T : Any> : BaseFragment() {

    protected var adapter: BaseSupportAdapter<T>? = null

    override fun getLayout(): Int = R.layout.fragment_list

    override fun initData() {
        initRclView()
    }

    protected open fun initRclView() {
        rvMain?.layoutManager = LinearLayoutManager(activity)
    }

    //////////////////////////////////////////////////////////////////////////////////////

    override fun onResume() {
        super.onResume()
        if (BackResult.retrieve(javaClass.simpleName) == true) {
            onReloadData()
        }
    }

    //////////////////////////////////////////////////////////////////////////////////////

    protected open fun setData(data: MutableList<T>) {
        if (rvMain?.adapter == null) {
            baseInitAdapter()
        } else {
            setItems(data)
        }
    }

    protected open fun baseInitAdapter() {
        initAdapter()
        rvMain?.adapter = adapter
        afterAddAdapter()
    }

    protected open fun afterAddAdapter() {
        //
    }

    protected open fun setItems(data: MutableList<T>) {
        adapter?.list = data
    }

    ///////////////////////////////////////////////////////////////////////////////////

    override fun tryLoadAgain() {
        onReloadData()
    }

    protected open fun clearItems() {
        adapter?.list?.clear()
        adapter?.notifyDataSetChanged()
    }

    override fun onDestroy() {
        super.onDestroy()
        adapter = null
        rvMain?.layoutManager = null
    }

    //////////////////////////////////////////////////////////////////////////////////////

    protected abstract fun loadData()
    protected abstract fun initAdapter()
}