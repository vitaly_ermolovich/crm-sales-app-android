package ru.salesap.crm.ui.model.base

import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.channels.ProducerScope
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.channels.produce
import org.kodein.di.generic.instance
import retrofit2.Response
import ru.salesap.crm.App
import ru.salesap.crm.net.Api
import ru.salesap.crm.net.response.ErrorResponse
import ru.salesap.crm.util.SettingPrefs
import java.util.*

abstract class BaseUseCase {

    fun <T> produceActions(f: suspend ProducerScope<Action<T>>.() -> Unit): ReceiveChannel<Action<T>> =
        GlobalScope.produce(block = f)

    protected fun getError(result: Response<*>): Throwable {
        val jsonAdapter: JsonAdapter<ErrorResponse> =
            Moshi.Builder().build().adapter<ErrorResponse>(ErrorResponse::class.java)
        val responseError = result.errorBody()?.let {
            jsonAdapter.fromJson(it.string())
        }
        return Throwable(responseError?.error ?: "Unknown error")
    }

    suspend fun <T> ProducerScope<Action<T>>.send(f: T.() -> T) = send(Action(f))
}

abstract class BaseApiUseCase : BaseUseCase() {

    companion object {
        const val LIMIT_PAGE = 20
        const val LIMIT_PAGE_MIDDLE = 10
        const val OK = 200
        const val ERROR_AUTH = 401
        const val TYPE_JSON = "application/json"
    }

    protected val api: Api by App.kodein.instance()
    protected val setting: SettingPrefs by App.kodein.instance()

    protected fun getLang(): String? = Locale.getDefault().language

    class Result<T>(val data: T? = null, val error: Throwable? = null, val code: Int? = null)
}
