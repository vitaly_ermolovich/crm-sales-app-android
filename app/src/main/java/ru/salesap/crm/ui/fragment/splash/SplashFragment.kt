package ru.salesap.crm.ui.fragment.splash

import android.view.animation.AnimationUtils
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import kotlinx.android.synthetic.main.fragment_splash.*
import ru.salesap.crm.R
import ru.salesap.crm.ui.fragment.base.BaseFragment
import ru.salesap.crm.ui.model.base.ViewStateStore
import ru.salesap.crm.ui.model.splash.SplashUseCase
import ru.salesap.crm.ui.model.splash.SplashViewModel
import ru.salesap.crm.ui.model.splash.SplashViewModelFactory
import ru.salesap.crm.ui.model.splash.SplashViewState
import ru.salesap.crm.util.navigateSplash

class SplashFragment : BaseFragment() {

    override fun getLayout(): Int = R.layout.fragment_splash

    override fun initData() {
        viewModel = ViewModelProvider(this, SplashViewModelFactory).get(SplashViewModel::class.java)
    }

    override fun initListeners() {
        super.initListeners()

        getViewModel<SplashViewModel>()
            .getStore<ViewStateStore<SplashViewState>>()
            .observe(this) {

                it.message?.let { text ->
                    showMessage(text)
                }

                if (it.openNext != SplashUseCase.EMPTY) {
                    openNext(it.openNext)
                    return@observe
                }
                startAnim()
            }
    }

    private fun showMessage(message: String) {
        if (message.isEmpty()) {
            return
        }
        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show()
    }

    private fun startAnim() {
        imgLoad?.startAnimation(AnimationUtils.loadAnimation(activity, R.anim.splash_anim))
        txtTitle?.startAnimation(AnimationUtils.loadAnimation(activity, R.anim.splash_anim))
    }

    private fun openNext(res: Int) {
        navigateSplash(
            R.id.action_splashFragment_to_logInFragment
        )
    }

}
