package  ru.salesap.crm.ui.model.splash

import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.delay
import ru.salesap.crm.ui.model.base.Action
import ru.salesap.crm.ui.model.base.BaseUseCase

class SplashUseCase : BaseUseCase() {

    companion object {
        const val EMPTY = -1
    }

    fun startWork(): ReceiveChannel<Action<SplashViewState>> = produceActions {
        send { copy(startAnim = true, openNext = EMPTY) }
        delay(2500L)
        send { copy(startAnim = false, openNext = 0) }
    }

}