package ru.salesap.crm.ui.fragment.base

import android.graphics.BitmapFactory
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.browser.customtabs.CustomTabsIntent
import androidx.core.content.ContextCompat
import androidx.core.net.toUri
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_list.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import ru.salesap.crm.R
import ru.salesap.crm.activity.source.CustomTabActivityHelper
import ru.salesap.crm.activity.source.WebViewFallback
import ru.salesap.crm.ui.model.base.BaseViewModel
import ru.salesap.crm.ui.model.base.ViewStateStore
import ru.salesap.crm.util.onBack

abstract class BaseFragment : Fragment() {

    protected open lateinit var viewModel: BaseViewModel
    protected lateinit var origin: String


    protected val watcher: TextWatcher by lazy {
        object : TextWatcher {

            private var searchFor = ""

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                val searchText = s.toString().trim()
                if (searchText == searchFor) {
                    return
                }
                val increase = searchFor.length < searchText.length
                searchFor = searchText

                GlobalScope.launch {
                    delay(300)
                    if (searchText != searchFor) {
                        return@launch
                    }
                    activity?.runOnUiThread {
                        changeText(increase)
                    }
                }
            }

            override fun afterTextChanged(s: Editable?) = Unit
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) =
                Unit
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? =
        inflater.inflate(getLayout(), container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initData()
        initListeners()
        onRestore(savedInstanceState, arguments)
    }

    protected open fun onRestore(savedInstanceState: Bundle?, arguments: Bundle?) {
        //
    }

    //////////////////////////////////////////////////////////////////////////////////////////

    override fun onDestroy() {
        super.onDestroy()
        if (::viewModel.isInitialized) {
            val st = getViewModel<BaseViewModel>().getStore<ViewStateStore<*>>()
            st.unsubscribe(this)
            st.cancel()
        }
    }

    //////////////////////////////////////////////////////////////////////////////////////////

    protected open fun initListeners() {
        btnBack?.setOnClickListener {
            onBack()
        }
        btnTryAgain?.setOnClickListener {
            tryLoadAgain()
        }
    }

    protected open fun changeText(increase: Boolean) {
        //
    }

    //////////////////////////////////////////////////////////////////////////////////////////

    protected open fun showHideProgress(isShowHide: Boolean) {
        activity?.runOnUiThread {
            pbLoad?.isVisible = isShowHide
        }
    }

    open fun showHideNoData(isEmpty: Boolean) {

        if (llError?.isVisible == isEmpty) {
            return
        }

        activity?.runOnUiThread {
            llError?.post {
                llError?.isVisible = isEmpty
            }
        }
    }

    //////////////////////////////////////////////////////////////////////////////////////////

    open fun showMessageError(message: String?, logOrToast: Boolean? = false) {
        if (message.isNullOrEmpty()) {
            return
        }

        if (logOrToast == true) {
            Log.e(javaClass.simpleName, message)
            return
        }

        activity?.runOnUiThread {
            Toast.makeText(activity ?: return@runOnUiThread, message, Toast.LENGTH_SHORT).show()
        }
    }

    //////////////////////////////////////////////////////////////////////////////////////////

    open fun tryLoadAgain() {
        //
    }

    //////////////////////////////////////////////////////////////////////////////////////////

    open fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        //
    }

    //////////////////////////////////////////////////////////////////////////////////////////

    protected fun openExternalLink(url: String, title: String) {
        val intent = CustomTabsIntent.Builder()
            .setToolbarColor(ContextCompat.getColor(activity ?: return, R.color.white))
            .setStartAnimations(
                activity ?: return,
                R.anim.slide_out_right,
                R.anim.slide_out_left_fast
            )
            .setExitAnimations(activity ?: return, R.anim.slide_in_right_slow, R.anim.slide_in_left)
            .setCloseButtonIcon(
                BitmapFactory.decodeResource(
                    resources,
                    R.drawable.bg_selector_back_white
                )
            )
            .setShowTitle(true)
            .build()

        CustomTabActivityHelper.openCustomTab(
            activity ?: return, intent, url.toUri(),
            WebViewFallback(object : WebViewFallback.SourceListener {
                override fun openSource() {
                    openSource(url, title)
                }
            })
        )
    }

    protected fun openSource(url: String, title: String? = getString(R.string.app_name)) {
/*
        navigateActivity(
            bundle = bundleOf(
                BaseViewModel.ARGUMENT_ID to url,
                BaseViewModel.ARGUMENT_EXTRA to title
            ),
            navHost = if (activity is OtherActivity) R.id.nav_other_host_fragment else R.id.nav_base_host_fragment
        )
*/
    }

    //////////////////////////////////////////////////////////////////////////////////////////

    open fun onReloadData() {
        //
    }

    abstract fun getLayout(): Int
    abstract fun initData()

    fun <V : Any> getViewModel(): V = viewModel as V
}
