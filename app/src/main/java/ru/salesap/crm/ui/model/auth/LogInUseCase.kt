package  ru.salesap.crm.ui.model.auth

import kotlinx.coroutines.channels.ReceiveChannel
import ru.salesap.crm.ui.model.base.Action
import ru.salesap.crm.ui.model.base.BaseUseCase

class LogInUseCase : BaseUseCase() {

    fun startWork(): ReceiveChannel<Action<LogInViewState>> = produceActions {
        send { copy(startAnim = true) }
    }

}