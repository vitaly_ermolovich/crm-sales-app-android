package ru.salesap.crm.ui.fragment.source

import android.annotation.SuppressLint
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.core.view.isVisible
import kotlinx.android.synthetic.main.fragment_source.*
import ru.salesap.crm.R
import ru.salesap.crm.ui.fragment.base.BaseFragment
import ru.salesap.crm.ui.model.base.BaseViewModel


class SourceFragment : BaseFragment() {

    override fun getLayout(): Int = R.layout.fragment_source

    @SuppressLint("SetJavaScriptEnabled")
    override fun initData() {
        wvMain?.webViewClient = AboutWebViewClient()
        wvMain?.settings?.javaScriptEnabled = true
        wvMain?.settings?.loadWithOverviewMode = true
        wvMain?.settings?.useWideViewPort = true
        wvMain?.settings?.setSupportZoom(true)
        wvMain?.settings?.builtInZoomControls = true

        toolbar?.title = arguments?.getString(BaseViewModel.ARGUMENT_EXTRA)
        toolbar?.setNavigationIcon(R.drawable.bg_selector_back_white)
        toolbar?.setNavigationOnClickListener {
            activity?.onBackPressed()
        }

        wvMain?.loadUrl(arguments?.getString(BaseViewModel.ARGUMENT_ID))

    }

    private inner class AboutWebViewClient : WebViewClient() {

        override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
            pbLoad?.isVisible = true
            return true
        }

        override fun onPageCommitVisible(view: WebView?, url: String?) {
            super.onPageCommitVisible(view, url)
            pbLoad?.isVisible = false
        }

        override fun onPageFinished(view: WebView, url: String) {
            super.onPageFinished(view, url)
            if (view.originalUrl == null) {
                view.loadUrl(url)
            }
        }
    }
}