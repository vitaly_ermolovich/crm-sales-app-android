package ru.salesap.crm.ui.fragment.base

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.fragment_list_refresh.*
import ru.salesap.crm.adapter.base.EndlessScrollListener
import ru.salesap.crm.ui.model.base.BaseApiUseCase

abstract class BaseEndlessListFragment<T : Any> : BaseListFragment<T>() {

    private var endLess: EndlessScrollListener? = null

    override fun initData() {
        super.initData()
        initEndless()
    }

    protected open fun initEndless() {
        endLess = object : EndlessScrollListener(rvMain?.layoutManager as LinearLayoutManager) {
            override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView?) {
                if (adapter?.itemCount == 0) {
                    return
                }
                loadNext()
            }
        }
    }

    override fun setItems(data: MutableList<T>) {
        if (adapter?.list.isNullOrEmpty()) {
            endLess?.setCurrentPage(data.size / BaseApiUseCase.LIMIT_PAGE, data.size - 1)
        }
        super.setItems(data)
    }

    protected open fun loadNext() {
        //
    }

    override fun onReloadData() {
        //
    }

    override fun clearItems() {
        endLess?.resetState()
        super.clearItems()
    }

    override fun afterAddAdapter() {
        rvMain?.addOnScrollListener(endLess ?: return)
    }

    override fun onDestroy() {
        super.onDestroy()
        endLess?.clearManager()
        endLess = null

    }
}