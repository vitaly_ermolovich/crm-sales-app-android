package ru.salesap.crm.ui.model.base

import androidx.lifecycle.ViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.cancel

abstract class BaseViewModel : ViewModel() {

    companion object {
        const val ARGUMENT_ID = "argument_id"
        const val ARGUMENT_EXTRA = "argument_extra"
    }

    protected lateinit var store: CoroutineScope

    fun <V : Any> getStore(): V = store as V

    override fun onCleared() {
        store.cancel()
    }

    abstract fun loadData()
    abstract fun getState(): Any
}