package ru.salesap.crm.ui.fragment.auth

import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import ru.salesap.crm.R
import ru.salesap.crm.ui.fragment.base.BaseFragment
import ru.salesap.crm.ui.model.auth.LogInViewModel
import ru.salesap.crm.ui.model.auth.LogInViewModelFactory
import ru.salesap.crm.ui.model.auth.LogInViewState
import ru.salesap.crm.ui.model.base.ViewStateStore

class LogInFragment : BaseFragment() {

    override fun getLayout(): Int = R.layout.fragment_log_in

    override fun initData() {
        viewModel = ViewModelProvider(this, LogInViewModelFactory).get(LogInViewModel::class.java)
    }

    override fun initListeners() {
        super.initListeners()

        getViewModel<LogInViewModel>()
            .getStore<ViewStateStore<LogInViewState>>()
            .observe(this) {

                it.message?.let { text ->
                    showMessage(text)
                }
            }
    }

    private fun showMessage(message: String) {
        if (message.isEmpty()) {
            return
        }
        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show()
    }

}
