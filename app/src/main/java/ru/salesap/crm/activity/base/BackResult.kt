package ru.salesap.crm.activity.base

import java.lang.ref.WeakReference

object BackResult {

    private var data: MutableMap<String, WeakReference<Boolean?>> = hashMapOf()

    private fun save(id: String, obj: Boolean?) {
        data[id] = WeakReference(obj)
    }

    fun clearAll() {
        data.clear()
    }

    fun save(vararg ids: String, obj: Boolean?) {
        ids.forEach {
            save(it, obj)
        }
    }

    fun retrieve(id: String): Boolean? {
        val result = data[id]?.get()
        result?.let {
            data.remove(id)
        }
        return result
    }
}