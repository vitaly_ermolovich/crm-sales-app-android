package ru.salesap.crm.activity.base

import android.content.Intent
import android.os.Bundle
import android.os.StrictMode
import android.provider.Settings
import androidx.appcompat.app.AppCompatActivity
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch
import org.kodein.di.generic.instance
import ru.salesap.crm.App
import ru.salesap.crm.BuildConfig
import ru.salesap.crm.net.Api
import ru.salesap.crm.ui.fragment.base.BaseFragment
import ru.salesap.crm.util.SettingPrefs

abstract class BaseActivity : AppCompatActivity() {

    protected val setting: SettingPrefs by App.kodein.instance()
    protected val api: Api by App.kodein.instance()

    protected fun saveDeviceId() {
        GlobalScope.launch {
            saveDeviceUuid()
        }
    }

    private suspend fun saveDeviceUuid(): SettingPrefs = coroutineScope {
        async {
            setting.setDevice(
                Settings.Secure.getString(contentResolver, Settings.Secure.ANDROID_ID)
            )
        }
    }.await()


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        supportFragmentManager.fragments.forEach { nav ->
            nav.childFragmentManager.fragments.forEach {
                it?.onActivityResult(requestCode, resultCode, data)
            }
        }
    }

    //////////////////////////////////// StrictMode //////////////////////////////////////////////////////

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getLayout())
        enableStrictMode()
    }

    abstract fun getLayout(): Int

    private fun enableStrictMode() {
        if (BuildConfig.DEBUG) {
            val threadPolicy = StrictMode.ThreadPolicy.Builder()
                .detectAll()
                .penaltyLog()
                .build()
            StrictMode.setThreadPolicy(threadPolicy)
        }
    }


    private fun reloadData() {
        supportFragmentManager.fragments.forEach { nav ->
            nav.childFragmentManager.fragments.forEach {
                if (it is BaseFragment?) {
                    it?.onReloadData()
                }
            }
        }
    }
}