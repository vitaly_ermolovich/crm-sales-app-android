package ru.salesap.crm.activity.source

import android.app.Activity
import android.net.Uri
import android.os.Bundle

import androidx.browser.customtabs.CustomTabsClient
import androidx.browser.customtabs.CustomTabsIntent
import androidx.browser.customtabs.CustomTabsServiceConnection
import androidx.browser.customtabs.CustomTabsSession

import ru.salesap.crm.activity.source.shared.CustomTabsHelper
import ru.salesap.crm.activity.source.shared.ServiceConnection
import ru.salesap.crm.activity.source.shared.ServiceConnectionCallback

class CustomTabActivityHelper : ServiceConnectionCallback {
    private var mCustomTabsSession: CustomTabsSession? = null
    private var mClient: CustomTabsClient? = null
    private var mConnection: CustomTabsServiceConnection? = null
    private var mConnectionCallback: ConnectionCallback? = null

    val session: CustomTabsSession?
        get() {
            if (mClient == null) {
                mCustomTabsSession = null
            } else if (mCustomTabsSession == null) {
                mCustomTabsSession = mClient?.newSession(null)
            }
            return mCustomTabsSession
        }

    fun unbindCustomTabsService(activity: Activity) {
        activity.unbindService(mConnection ?: return)
        mClient = null
        mCustomTabsSession = null
        mConnection = null
    }


    fun setConnectionCallback(connectionCallback: ConnectionCallback) {
        this.mConnectionCallback = connectionCallback
    }

    fun bindCustomTabsService(activity: Activity) {
        mClient?.let {
            val packageName = CustomTabsHelper.getPackageNameToUse(activity) ?: return
            mConnection = ServiceConnection(this)
            CustomTabsClient.bindCustomTabsService(activity, packageName, mConnection)
        }
   }

    fun mayLaunchUrl(uri: Uri, extras: Bundle, otherLikelyBundles: List<Bundle>): Boolean {
        if (mClient == null) return false
        val session = session ?: return false
        return session.mayLaunchUrl(uri, extras, otherLikelyBundles)
    }

    override fun onServiceConnected(client: CustomTabsClient) {
        mClient = client
        mClient?.warmup(0L)
        mConnectionCallback?.onCustomTabsConnected()
    }

    override fun onServiceDisconnected() {
        mClient = null
        mCustomTabsSession = null
        mConnectionCallback?.onCustomTabsDisconnected()
    }


    interface ConnectionCallback {

        fun onCustomTabsConnected()

        fun onCustomTabsDisconnected()
    }

    interface CustomTabFallback {
        fun openUri(activity: Activity, uri: Uri)
    }

    companion object {

        fun openCustomTab(
            activity: Activity,
            customTabsIntent: CustomTabsIntent,
            uri: Uri,
            fallback: CustomTabFallback?
        ) {
            val packageName = CustomTabsHelper.getPackageNameToUse(activity)
            if (packageName == null) {
                fallback?.openUri(activity, uri)
            } else {
                customTabsIntent.intent.setPackage(packageName)
                customTabsIntent.launchUrl(activity, uri)
            }
        }
    }

}


