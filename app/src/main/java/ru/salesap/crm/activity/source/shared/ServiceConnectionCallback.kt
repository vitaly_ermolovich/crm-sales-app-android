package ru.salesap.crm.activity.source.shared

import androidx.browser.customtabs.CustomTabsClient

interface ServiceConnectionCallback {

    fun onServiceConnected(client: CustomTabsClient)

    fun onServiceDisconnected()
}
