package ru.salesap.crm.activity

import android.os.Bundle
import androidx.navigation.findNavController
import ru.salesap.crm.R
import ru.salesap.crm.activity.base.BaseActivity

class MainActivity : BaseActivity() {

    override fun getLayout() = R.layout.activity_main

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        saveDeviceId()
    }

    override fun onSupportNavigateUp() = findNavController(R.id.nav_base_host_fragment).navigateUp()
}
