package ru.salesap.crm.activity.source

import android.app.Activity
import android.net.Uri
import ru.salesap.crm.activity.source.CustomTabActivityHelper.CustomTabFallback

class WebViewFallback(private val listener: SourceListener) : CustomTabFallback {
    override fun openUri(activity: Activity, uri: Uri) {
        listener.openSource()
    }

    interface SourceListener {
        fun openSource()
    }
}